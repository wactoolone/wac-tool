package com.wearechurch.tool.configuration;

import org.springframework.beans.factory.annotation.Value;

import com.wearechurch.tool.annotation.ComponentConfiguration;

@ComponentConfiguration
public class WacProperty {

	@Value("${spring.security.user.name}")
	private String securityUserName;

	@Value("${spring.security.user.password}")
	private String securityUserPassword;

	@Value("${spring.application.name}")
	private String springApplicationName;

	public String getSecurityUserName() {
		return securityUserName;
	}

	public String getSecurityUserPassword() {
		return securityUserPassword;
	}

	public String getSpringApplicationName() {
		return springApplicationName;
	}

}
