package com.wearechurch.tool.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.wearechurch.tool.dto.Response;
import com.wearechurch.tool.enumerator.Reply;
import com.wearechurch.tool.exception.WacException;
import com.wearechurch.tool.service.WacService;

@ControllerAdvice
public class AdviceController {

	private final Logger logger = LoggerFactory.getLogger(AdviceController.class);

	@Autowired
	private WacService service;

	private final ResponseEntity<Response<Void>> buildResponse(final Exception exception, final Reply reply) {
		logger.error("Code: {} | ClassName: {} | Message: {}", reply.getCode(), exception.getClass().getName(),
				exception.getLocalizedMessage());
		return ResponseEntity.ok(service.reply(reply));
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Response<Void>> exception(final Exception exception) {
		return buildResponse(exception, Reply.EXCEPTION);
	}

	@ExceptionHandler(WacException.class)
	public ResponseEntity<Response<Void>> wacException(final WacException exception) {
		return buildResponse(exception, exception.getReply());
	}

}