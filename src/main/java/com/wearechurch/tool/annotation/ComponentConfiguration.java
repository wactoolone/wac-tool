package com.wearechurch.tool.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@Configuration
@RefreshScope
@Retention(RetentionPolicy.RUNTIME)
public @interface ComponentConfiguration {
}