package com.wearechurch.tool.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@ComponentApplication
@EnableDiscoveryClient
@Retention(RetentionPolicy.RUNTIME)
public @interface ComponentDiscoveryApplication {

}