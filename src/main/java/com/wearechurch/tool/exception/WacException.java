package com.wearechurch.tool.exception;

import com.wearechurch.tool.enumerator.Reply;

public class WacException extends RuntimeException {

	private static final long serialVersionUID = -2076280920102349549L;

	private final Reply reply;
	private final String localizedMessage;

	public WacException(final Reply reply, final String localizedMessage) {
		this.reply = reply;
		this.localizedMessage = localizedMessage;
	}

	@Override
	public String getLocalizedMessage() {
		return localizedMessage;
	}

	public Reply getReply() {
		return reply;
	}

}
