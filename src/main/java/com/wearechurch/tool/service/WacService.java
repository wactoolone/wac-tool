package com.wearechurch.tool.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wearechurch.tool.configuration.WacProperty;
import com.wearechurch.tool.dto.Response;
import com.wearechurch.tool.enumerator.Reply;

@Component
public class WacService {

	@Autowired
	private WacProperty property;

	public Response<Void> reply() {
		return reply(Reply.OK);
	}

	public <B> Response<B> reply(final B body) {
		return reply(body, Reply.OK);
	}

	private final <B> Response<B> reply(final B body, final Reply reply) {
		final Response<B> response = new Response<>();
		final String[] name = property.getSpringApplicationName().split("-");
		response.setApplication(name[name.length - 1]);
		response.setBody(body);
		response.setCode(reply.getCode());
		response.setMessage(reply.getMessage());
		return response;
	}

	public Response<Void> reply(final Reply reply) {
		return reply(null, reply);
	}
}
