package com.wearechurch.tool.enumerator;

public enum Reply {
	EXCEPTION(-1, "Tenemos un problema interno, intentalo más tarde."), OK(0, "OK"), CORRUPT(3, "Datos incorrectos."),
	EXISTS(5, "Violación de integridad al intentar persistir el registro."), NONEXISTENT(14, "No existe el registro."),
	OUTDATED(16, "Descarga una nueva versión de la app, la que tienes está obsoleta."),
	UNAVAILABLE(36, "Servicio no disponible."), UPGRADABLE(39, "Hay una nueva versión disponible de la app.");

	private Integer code;
	private String message;

	private Reply(final Integer code, final String message) {
		this.code = code;
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

}