package com.wearechurch.tool.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(value = Include.NON_EMPTY)
public class Response<B> {

	private String application;
	private B body;
	private Integer code;
	private String message;

	public String getApplication() {
		return application;
	}

	public B getBody() {
		return body;
	}

	public Integer getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public void setApplication(final String application) {
		this.application = application;
	}

	public void setBody(final B body) {
		this.body = body;
	}

	public void setCode(final Integer code) {
		this.code = code;
	}

	public void setMessage(final String message) {
		this.message = message;
	}

}
